$SessionVideoServiceURL = 'http://localhost:3000/api/v1'
while (1) {
    echo "Waiting for network connectivity"
    $code = ''

    do {
       $resp = try { Invoke-WebRequest $SessionVideoServiceURL } catch { $_.Exception.Response }
       $code = $resp.StatusCode
       Start-Sleep -s 2
    }
    until($code -eq 'NotFound')

    echo "Starting desktop capture"
    pipenv install
    pipenv run python .\capture.py
}
