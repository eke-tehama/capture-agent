# Capture Agent
This is a really rough and quick capture agent that runs on desktops and uploads segments to the recording v2 session video service. It is written in python, and has been tested on MacOS and Windows

Here are the steps required to get this running on a desktop:

## Windows Workspace
Perform the following instructions on a windows workspace of your choosing. The instructions below were tested on a `Win 10 Pro` bundle.
### 1.0 Install Dependencies
#### FFMpeg
* Download static ffmpeg pre-build binaries for Windows here: https://ffmpeg.org/download.html#build-windows
* Extract and save the ffmpeg folder to `C:\`
* Remember this path (eg `C:\ffmpeg`)
#### Direct Show Video Capture Device
* Install the `screen-capture-recorder` direct show video capture device from here: https://github.com/rdp/screen-capture-recorder-to-video-windows-free
#### Python 3.8
* Install Python 3.8 from here: https://www.python.org/downloads/windows/ (you want the executable installer)
#### Pipenv
1. Save this file: `https://raw.githubusercontent.com/pypa/pipenv/master/get-pipenv.py`
2. Open a powershell window (run as administrator) and run this command:
  `python /path/to/file/downloaded/from/step/1/get-pipenv.py`

### 2.0 Deploy Capture Agent
* Zip this folder into an archive
* Copy the archive to the windows workspace
* Extract the archive into a folder on the workspace eg `C:\capture-agent`

### 3.0 Configure Capture Agent
#### Update YAML File
* On the workspace, edit the file at `C:\capture-agent\config.yaml`
* Set the `server uri` field to the accurate URI for the session video service load balancer. Eg `http://vela-perf-svs.tehama.amazonlb.aws.com/api/v1`
* Set the `user id`, `org id`, `room id`, and `workspace id` to the appropriate values for your env and room. **It is important to get these values correct, if you bungle this step you may not see the recordings when you log into your Tehama room**
* Set `ffmpeg.command.binary` to the path where you installed ffmpeg. This is the path to the exact ffmpeg binary. Eg `C:\ffmpeg\bin\ffmpeg`
#### Update Powershell Launcher Script
* On the workspace, edit the file at `C:\capture-agent\launcher.ps1`. Update the `$SessionVideoServiceURL` variable to be the `server uri` value from the `config.yaml` file

### 4.0 Create Scheduled Task
* On the workspace, create a scheduled task to run the `C:\capture-agent\launcher.ps1` launcher file whenever a user logs in

### 5.0 Other Notes
#### FFMpeg Command Breakdown
FFMpeg commands are typically broken into two parts. The inputs, and the outputs. The inputs typically come before the outputs. A basic skeleton would look like this:

`ffmpeg [INPUT PARAMS...] [OUTPUT PARAMS...]`

For instance, the following command transcodes an `mp4` file into `webm`

`ffmpeg -f mp4 -i file.mp4 -f webm file.webm`

But can be shortened to:

`ffmpeg -i file.mp4 file.webm`

The ffmpeg configuration for the capture agent splits out the inputs across different operating systems, but they all use the same output (`MPEG-DASH`). Here is the relevant section of the `config.yaml` file:

```
ffmpeg:
  command:
    binary: ffmpeg
    input-args:
      win32: -f dshow -framerate 10 -rtbufsize 1024M -i video=screen-capture-recorder
      darwin: -f avfoundation -r 10 -i 1
      linux: -f x11grab -framerate 10 -i :0.0
    output-args: -an -c:v libvpx-vp9 -pix_fmt yuv420p -keyint_min 60 -g 60 -speed 10 -tile-columns 4 -frame-parallel 1 -threads 8 -static-thresh 0 -max-intra-rate 300 -deadline realtime -lag-in-frames 0 -error-resilient 1 -f webm -dash 1 -vf scale=960:-1 -b:v 250k -dash 1 -seg_duration 5 -remove_at_exit 0 -method PUT -init_seg_name init.webm -media_seg_name $Number$.$ext$ -dash_segment_type webm -f dash
```

You can see the input args for the different platforms. All input arguments use a screen capture video input device on the different platforms. The capture agent simply selects the right `input-args` for your platform, then concatenates that with the `output-args`. A complete ffmpeg command for windows will look like this (comments/explanations also below):

```
ffmpeg \
  -f dshow \      # using the direct show for screen capture on windows
    -framerate 10 \       # !!very important!! without this, screen capture input frame rates are too high for the encoder
    -rtbufsize 1024M \    # tries to pin memory usage
    -i video=screen-capture-recorder \
  -an \                   # disable audio output
  -c:v libvpx-vp9 \       # set output codec to vp9
    -pix_fmt yuv420p \    # set output pixel format
    -keyint_min 60 \
    -g 60 \
    -speed 10 \           # higher encoder speed = less output quality but less mem & cpu usage in ffmpeg
    -tile-columns 4 \
    -frame-parallel 1 \
    -threads 8 \
    -static-thresh 0 \
    -max-intra-rate 300 \
    -deadline realtime \
    -lag-in-frames 0 \
    -error-resilient 1 \
    -f webm                   # webm output configuration for the video segments
      -dash 1 \               # mpeg-dash output config for video segments
        -vf scale=960:-1 \    # downscale the output segments to 540p
        -b:v 250k \           # set output segment bit rate
      -dash 1 \               # mpeg-dash output config for the output MPD file
        -seg_duration 5 \
        -remove_at_exit 0 \
        -method PUT \
        -init_seg_name init.webm \
        -media_seg_name $Number$.$ext$ \
        -dash_segment_type webm \
    -f dash http://server-uri/manifest.mpd
```

#### Odds & Ends
* I was not able to get `gdigrab` screen capture on Windows to work, it always seemed to have too high of a framerate. This throws off everything else down the pipeline
* When you try this out on a new workspace, the first few tries will fail due to windows updates and WSA updates.
