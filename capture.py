import signal
import time

from yaml import load, dump
try:
    from yaml import CLoader as Loader
except ImportError:
    from yaml import Loader

from capture_agent.live_session import LiveSession


if __name__ == '__main__':
    config = None
    with open('config.yaml') as f:
        config = load(f, Loader=Loader)

    live_sesh = LiveSession(config)

    def handler(signum, frame):
        print('Signal handler called with signal', signum, '. Quitting ffmpeg')
        print('Telling ffmpeg to quit over stdin')
        live_sesh.process.communicate(input='q'.encode('utf-8'))
        print('Ffmpeg quit command returned')
        
    signal.signal(signal.SIGTERM, handler)
    signal.signal(signal.SIGINT, handler)

    session_id = live_sesh.start()
    live_sesh.run_ffmpeg(session_id)

    while live_sesh.process.poll() is None:
        time.sleep(1)

    print('Ffmpeg exited')
    live_sesh.logfile.close()

    live_sesh.end(session_id)
