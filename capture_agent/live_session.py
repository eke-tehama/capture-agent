import subprocess
import sys

from .client import SessionVideoServiceClient

class LiveSession:
    def __init__(self, config):
        self.config = config
        self.client = SessionVideoServiceClient(self.config)

    def run_ffmpeg(self, session_id):
        print('Running ffmpeg')
        platform = sys.platform

        input_args = self.config['ffmpeg']['command']['input-args'][platform]

        if not input_args:
            raise ValueError('Recording does not support this platform: %s' % platform)

        output_args = '%(output)s %(uri)s/dashupload/%(sessionid)s/playlist' % {'output': self.config['ffmpeg']['command']['output-args'], 'uri': self.config['server']['uri'], 'sessionid': session_id}

        args = '%s %s %s' % (self.config['ffmpeg']['command']['binary'], input_args, output_args)
        print(args)
        args = args.split(' ')

        self.logfile = open('logs/ffmpeg.log', 'w')

        self.process = subprocess.Popen(args, stdin=subprocess.PIPE, stdout=self.logfile, stderr=self.logfile)

    def start(self):
        print('Starting recording session')
        response = self.client.start_video_session()

        if response.status_code >= 300:
            raise RuntimeError('Failed to start session' + response.text)

        return response.json()['id']

    def end(self, session_id):
        print('Ending session', session_id)
        response = self.client.end_video_session(session_id)

        if response.status_code >= 300:
            raise RuntimeError('Failed to end session' + response.text)