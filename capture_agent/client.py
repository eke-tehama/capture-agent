import requests

class SessionVideoServiceClient:
    def __init__(self, config):
        self.config = config

    def start_video_session(self):
        payload = {
            'userName': self.config['user']['id'],
            'workspaceId': self.config['workspace']['id'],
            'roomId': self.config['room']['id'],
            'orgId': self.config['org']['id']
        }

        return requests.post('%s/start' % (self.config['server']['uri']), data=payload)

    def end_video_session(self, session_id):
        return requests.post('%s/end/%s' % (self.config['server']['uri'], session_id))